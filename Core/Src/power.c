/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power.h"
#include "qubik.h"
#include "cmsis_os.h"
#include "error.h"
#include "watchdog.h"
#include "telemetry.h"

extern struct qubik hqubik;
extern ADC_HandleTypeDef hadc1;
extern osMutexId pwr_mtxHandle;
extern struct watchdog hwdg;

int
power_get_adc_value(int channel, uint16_t *value);

int
power_set_adc_channel(int channel);

/**
 * @brief Update power status structure from max17261 values
 * @param power_status
 * @return 0 on success or negative error code
 */
int
update_power_status(struct power_status *power_status)
{
	uint16_t adc_value;
	int ret = osMutexWait(pwr_mtxHandle, 1000);
	if (ret != osOK) {
		return -MTX_TIMEOUT_ERROR;
	}


	// Auxiliary voltage monitor
	ret = power_get_adc_value(POWER_CH_V_ALT, &adc_value);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}
	power_status->voltage_alt = __LL_ADC_CALC_DATA_TO_VOLTAGE(ADC_VREF, adc_value,
	                            hadc1.Init.Resolution) * POWER_V_ALT_COEFF;

	// MCU Temperature
	ret = power_get_adc_value(POWER_CH_TEMP, &adc_value);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}
	power_status->temperature_mcu = __LL_ADC_CALC_TEMPERATURE(
	                                        ADC_VREF, adc_value, hadc1.Init.Resolution);

	// RTC Battery voltage
	ret = power_get_adc_value(POWER_CH_V_RTC, &adc_value);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}
	power_status->rtc_vbat = __LL_ADC_CALC_DATA_TO_VOLTAGE(ADC_VREF, adc_value,
	                         hadc1.Init.Resolution) * 3;


	// Get power values from MAX17261
	if (max17261_init(&hqubik.hmax17261) == 0) {
		power_status->voltage = max17261_get_voltage(&hqubik.hmax17261);
		power_status->voltage_avg = max17261_get_average_voltage(
		                                    &hqubik.hmax17261);
		power_status->current = max17261_get_current(&hqubik.hmax17261);
		power_status->current_avg = max17261_get_average_current(
		                                    &hqubik.hmax17261);
		power_status->SOC = max17261_get_SOC(&hqubik.hmax17261);
		power_status->RepCap = max17261_get_reported_capacity(&hqubik.hmax17261);
		power_status->FullRepCap = max17261_get_full_reported_capacity(
		                                   &hqubik.hmax17261);
		power_status->temperature = max17261_get_temperature(&hqubik.hmax17261);
		power_status->temperature_die = max17261_get_die_temperature(
		                                        &hqubik.hmax17261);
		power_status->temperature_avg = max17261_get_average_temperature(
		                                        &hqubik.hmax17261);
		power_status->TTE = max17261_get_TTE(&hqubik.hmax17261);

		max17261_get_minmax_voltage(&hqubik.hmax17261,
		                            &power_status->voltage_min,
		                            &power_status->voltage_max);
		max17261_get_minmax_current(&hqubik.hmax17261,
		                            &power_status->current_min,
		                            &power_status->current_max);
		max17261_get_minmax_temperature(&hqubik.hmax17261,
		                                &power_status->temperature_min, &power_status->temperature_max);
	} else {
		osMutexRelease(pwr_mtxHandle);
		return -QUBIK_PWR_ERROR;
	}
	osMutexRelease(pwr_mtxHandle);
	return NO_ERROR;
}

/**
 * @brief Determine power state
 *
 * Evaluate all power parameters to determine if system is in a degraded power state
 * @param power_status
 * @return power_state_t
 */
power_state_t
evaluate_power_state(struct power_status *power_status)
{
	if (hqubik.status.power_mon_fail == 0) {
		// Power monitor functional
		if (hqubik.status.power_save == 0) {
			return (hqubik.power.SOC
			        < POWER_LOW_ENTRY_SOC_THRESHOLD)
			       && (hqubik.power.voltage_avg < POWER_LOW_ENTRY_V_THRESHOLD);
		} else {
			return (hqubik.power.SOC
			        < POWER_LOW_EXIT_SOC_THRESHOLD)
			       || (hqubik.power.voltage_avg < POWER_LOW_EXIT_V_THRESHOLD);
		}
	} else {
		// Power monitor failed
		if (hqubik.power.voltage_alt < POWER_BO_VOLTAGE) {
			return POWER_STATE_OK;
		}
		if (hqubik.status.power_save == 0) {
			return (hqubik.power.voltage_alt < POWER_LOW_ENTRY_V_THRESHOLD);
		} else {
			return (hqubik.power.voltage_alt < POWER_LOW_EXIT_V_THRESHOLD);
		}
	}
	return POWER_STATE_OK;
}

void
power_task()
{
	uint8_t wdgid = 0;
	watchdog_register(&hwdg, &wdgid, "power");
	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		hqubik.status.power_mon_fail = (update_power_status(&hqubik.power)
		                                != NO_ERROR);
		hqubik.status.power_save = evaluate_power_state(&hqubik.power) ==
		                           POWER_STATE_DEGRADED;
		osDelay(POWER_POLLING_INTERVAL);
	}
}

int
power_set_adc_channel(int channel)
{
	HAL_StatusTypeDef ret;
	ADC_ChannelConfTypeDef sConfig = {0};

	sConfig.Channel = channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_24CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	ret = HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK;
	return ret;
}

int
power_get_adc_value(int channel, uint16_t *value)
{
	HAL_StatusTypeDef ret = 0;
	// Set channel
	ret = power_set_adc_channel(channel);
	if (ret) {
		return ret;
	}
	// Calibrate ADC
	ret = HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	if (ret) {
		return ret;
	}
	// Start Conversion
	ret = HAL_ADC_Start(&hadc1);
	if (ret) {
		return ret;
	}
	// Wait for conversion
	ret = HAL_ADC_PollForConversion(&hadc1, 100);
	if (ret) {
		return ret;
	}
	// Read raw value
	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		*value = HAL_ADC_GetValue(&hadc1);
	}
	ret = HAL_ADC_Stop(&hadc1);
	return ret;
}
